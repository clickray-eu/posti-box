ready('.box-language-switcher .lang_list_class', function(element) {
	const languages = $(element).find('.lang_switcher_link');
	languages.each(function () {
		let languageName = $(this).text().split(' ');
		$(this).text(languageName[0]);
	})

	$(element).addClass('active');
});