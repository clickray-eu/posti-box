$('.boxy-row__item-content-accordion-button a').on('click', function() {
	// toggle accordion
	$(this).parents('.boxy-row__item-content-accordion').find('.boxy-row__item-content-accordion-body').toggleClass('boxy-row__item-content-accordion-body--open')
	$(this).parents('.boxy-row__item-content-accordion').find('.boxy-row__item-content-accordion-body').stop().slideToggle();
});