$('.accordion__header').on('click', function(e) {
	e.preventDefault();
	let target = $(this).attr('href');

	if ($(target).hasClass('accordion__content--open')) {
		$('.accordion__content').slideUp();
		$('.accordion__content').removeClass('accordion__content--open');
		$('.accordion__header').removeClass('accordion__header--active')
	} else {
		$('.accordion__content').slideUp();
		$('.accordion__content').removeClass('accordion__content--open');
		$('.accordion__header').removeClass('accordion__header--active')
		$(target).slideDown();
		$(target).addClass('accordion__content--open');
		$(this).addClass('accordion__header--active')
	}
});